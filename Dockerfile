FROM amazoncorretto:8

RUN yum update -y && \
    yum install -y maven tar gzip wget

# install tomcat
WORKDIR /tmp
RUN wget https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.62/bin/apache-tomcat-9.0.62.tar.gz && \
    tar -xzvf ./apache-tomcat-9.0.62.tar.gz && \
    mv ./apache-tomcat-9.0.62 /opt && \
    rm -rf ./*

WORKDIR /opt
RUN ln -s ./apache-tomcat-9.0.62 ./apache-tomcat

COPY . /app